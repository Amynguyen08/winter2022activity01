public class MethodsTest{
  public static void main (String[] args){
    int x = 5;
    // System.out.println(x);
    //methodNoInputNoReturn();
    // System.out.println(x);
    // System.out.println(x);
    // methodOneInputNoReturn(x);
    // System.out.println(x);
    //  methodTwoInputNoReturn(2.5,x);
    //  int j = methodNoInputReturnInt();
    // System.out.println(j);
    //double k = sumSquareRoot(9,5);
    // System.out.println(k);
    String s1 = "java";
    String s2 = "programming";
    // String.length();
   // System.out.println(s1.length());
    //System.out.println(s2.length());
   // System.out.println(SecondClass.addOne(50));
     //System.out.println(SecondClass.addTwo(50)); //totally wrong
    SecondClass sc = new SecondClass();
    System.out.println(sc.addTwo(50));
  }
  public static void methodNoInputNoReturn(){
    System.out.println("I'm in a method that takes no input and returns nothing");
    int x = 20;
    System.out.println(x);
  }
  public static void methodOneInputNoReturn(int number){
    number = number-5;
    System.out.println("Inside the method one input no returns " + number);
  }
  public static void methodTwoInputNoReturn(int x,double y){
    System.out.println(x);
    System.out.print(y);
  }
  public static int methodNoInputReturnInt(){
    return 5;
  }
  public static double sumSquareRoot(int x, int y){
    double k = x+y;
    k = Math.sqrt(k);
    return k;
  }
}
