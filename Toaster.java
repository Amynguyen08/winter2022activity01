	import java.util.Scanner;
	class Toaster{
		private String colour;
		private String brand;
		private double price;
		
	public void colourAndBrand(String colour,String brand){
		System.out.println("Your Toaster is "+colour+" made by "+brand);
	}
	public void withTax(double price){
		double newPrice = price*1.15;
		System.out.format("Your toaster with tax comes to a total of: "+ "%.2f", newPrice);
	}
//instance method 
	public void toastInflation(double newPrice){
		if(newPrice > 100){
			this.price = newPrice - 20;
		}
		else if(newPrice > 50){
			this.price = newPrice;
		}
else if (newPrice < 50){
		this.price = newPrice +10;
}
	}
	//setter methods
	public void setColour(String newColour){
		this.colour=newColour;
	}
	//deleted setBrand
	//bringing it back for "part 2: Adding a constructor qustion 4"
	public void setBrand(String newBrand){
		this.brand=newBrand;
	}
	
	public void setPrice(double newPrice){
		this.price=newPrice;
	}
	
	public String getColour(){
		return this.colour;
		}
	public String getBrand(){
		return this.brand;
					
	}
	public double getPrice(){
		return this.price;
				}
	//constructor
	public Toaster(String colour,String brand, double price){
		this.colour=colour;
		this.brand=brand;
		this.price=price;
	}

	}




