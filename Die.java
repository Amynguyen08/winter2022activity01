import java.util.Random;
public class Die{
    private Random rand = new Random();
    private int faceValue;


    public Die(){
        this.faceValue = 1;
    }
    public int getFaceValue(){
        return this.faceValue;
    }
    public int roll(){
        this.faceValue = rand.nextInt(6)+1;
        return this.faceValue;
    }
    }
