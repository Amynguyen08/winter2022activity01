public class Board{
    private Die dice1;
    private Die dice2;
    //represents value of sum rolled. 1 means sum has been rolled, 0 if not
    private boolean[] tiles;
    //boolean[] tiles = new Boolean[size]

public Board(){
    dice1 = new Die();
    dice2 = new Die();
    tiles = new boolean[12];
}
public String toString(){
        //return "Your dice has rolled a: " + roll();
        String result = " ";
        for(int i=0;i<tiles.length;i++){
            /*if tiles at position i is false, returns result of number
            +1 and a space*/
            if(!tiles[i]){
                result +=(i+1) +" ";
            }
            //if tile at position i is true, then returns x as turned
            else{
                result +="x ";
            }
            }
            //returns the array 
            return result;
        }
public boolean playATurn(){
    dice1.roll();
    dice2.roll();
    System.out.println("Dice 1= "+dice1.getFaceValue()+" "+"dice 2= "+dice2.getFaceValue());
    int sumOfDice = dice1.getFaceValue()+dice2.getFaceValue();
        if(!this.tiles[sumOfDice-1]){
            this.tiles[sumOfDice-1] = true;
            System.out.println("Closing tile with the same value as die one: "+ this.tiles[sumOfDice-1]);
            return false;
        }
         else if(!this.tiles[dice1.getFaceValue()-1]){
            this.tiles[dice1.getFaceValue()-1] = true;
            System.out.println("Closing tile with the same value as die one: "+ this.tiles[dice1.getFaceValue()]);
            return false;
        }
        else if(!this.tiles[dice2.getFaceValue()-1]){
            this.tiles[dice2.getFaceValue()-1] = true;
            System.out.println("Closing tile with the same value as die one: "+ this.tiles[dice2.getFaceValue()]);
            return false;
        }
        else{
            System.out.println("All the tiles for these values are already shut!");
            return true;
        }
                
    }
}


